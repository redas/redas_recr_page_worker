package com.cdgeekcamp.redas.recr_page_worker.component;

import com.cdgeekcamp.redas.lib.core.api.receivedParameter.RecrPage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;

/**
 * 招聘数据转换到表抽象接口
 */
abstract interface RecrPageBuildService {
    Logger log = LoggerFactory.getLogger(RecrPageBuildService.class);

    Integer build(RecrPage recrPage, Integer... relatedIds) throws NoSuchAlgorithmException, UnsupportedEncodingException;
}
