package com.cdgeekcamp.redas.recr_page_worker.component;

import com.cdgeekcamp.redas.db.model.PositionAdvantage;
import com.cdgeekcamp.redas.db.model.PositionAdvantageRepository;
import com.cdgeekcamp.redas.db.model.R_PositionAdvantage;
import com.cdgeekcamp.redas.db.model.R_PositionAdvantageRepository;
import com.cdgeekcamp.redas.lib.core.api.receivedParameter.RecrPage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Optional;

@Component
public class PositionAdvantageBuildServiceImpl implements RecrPageBuildService {
    @Autowired
    private PositionAdvantageRepository positionAdvantages;

    @Autowired
    private R_PositionAdvantageRepository r_positionAdvantages;

    public Integer build(RecrPage recrPage, Integer... relatedIds) {
        ArrayList<String> Advantages = recrPage.getAdvantage();

        for (String advantage : Advantages) {
            Optional<PositionAdvantage> positionAdvantageOpt = positionAdvantages.findByDescribe(advantage);
            PositionAdvantage positionAdvantage;

            if (positionAdvantageOpt.isEmpty())
                positionAdvantage = positionAdvantages.save(new PositionAdvantage(advantage));
            else
                positionAdvantage = positionAdvantageOpt.get();

            Integer advantageId = positionAdvantage.getId();
            Integer positionId = relatedIds[0];

            Optional<R_PositionAdvantage> r_positionAdvantageOpt =
                    r_positionAdvantages.findByAdvantageIdAndPositionId(advantageId, positionId);

            if (r_positionAdvantageOpt.isEmpty()) {
                r_positionAdvantages.save(new R_PositionAdvantage(advantageId, positionId));
                log.info(String.format("职位亮点：%s->职位：%s 关联不存在，新建中......", advantageId, positionId));
            } else
                log.info(String.format("职位亮点：%s->职位：%s 关联已经存在......", advantageId, positionId));
        }

        return 0;
    }
}
